import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { NodeComponent } from "./node/node.component";
import { readFileComponent } from "./file-load/read-file.component";
import { CommentComponent } from "./comment/comment.component";
import { TooltipModule } from "ng2-tooltip-directive";

@NgModule({
    declarations: [AppComponent, NodeComponent, readFileComponent, CommentComponent],
    imports: [BrowserModule, TooltipModule],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
