import { ChangeDetectionStrategy, Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "app-comment",
    templateUrl: "./comment.component.html",
    changeDetection:ChangeDetectionStrategy.OnPush
})
export class CommentComponent {
    @Output()
    public readonly output: EventEmitter<string> = new EventEmitter();
    @Output()
    public readonly hide: EventEmitter<any> = new EventEmitter();

    public value: string = "";

    public onInput(event: Event): void {
        const target = event.target as HTMLInputElement;
        this.value = target.value;
    }

    public saveComent() {
        this.output.emit(this.value);
        this.hide.next("");
    }

    public cancel() {
        this.value = "";
        this.hide.next("");
    }
}
