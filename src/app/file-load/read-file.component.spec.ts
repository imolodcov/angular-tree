import { ComponentFixture, TestBed } from "@angular/core/testing";

import { readFileComponent } from "./read-file.component";

describe("FileLoadComponent", () => {
    let component: readFileComponent;
    let fixture: ComponentFixture<readFileComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [readFileComponent]
        }).compileComponents();

        fixture = TestBed.createComponent(readFileComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
