import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from "@angular/core";
import { Node } from "../../src";
import { TreeService } from "../services/tree.service";

@Component({
    selector: "app-read-file",
    templateUrl: "./read-file.component.html",
    styleUrls: ["./read-file.component.css"],
    changeDetection:ChangeDetectionStrategy.OnPush
})
export class readFileComponent {
    public fileName = "Загрузите файл";
    public data: Node[] = [];

    private treeService: TreeService;
    private cdr: ChangeDetectorRef;

    constructor(treeService: TreeService, cdr:ChangeDetectorRef) {
        this.treeService = treeService;
        this.cdr =cdr
    }

    public onFileSelected(event: Event) {
        this.data = [];
        const target: HTMLInputElement = event.target as HTMLInputElement;
        const files = target.files ? target.files[0] : null;
        if (files) {
            if (["text/csv"].indexOf(files.type) < 0) {
                this.fileName = "Недопустимый тип файл";
            } else {
                this.fileName = files.name;
                let reader: FileReader = new FileReader();
                reader.readAsText(files);
                reader.onload = () => {
                    let csv = reader.result;
                    let readerCsv = (csv as string)
                        .split(/\r\n|\n/)
                        .filter((line) => line.length > 0)
                        .map((line: string) => {
                            const [id, name, parentId] = line.split(",");
                            return { id, name, parentId };
                        });
                    this.data = this.treeService.makeTree(readerCsv);
                    this.cdr.detectChanges()
                };
            }
        }
    }
}
