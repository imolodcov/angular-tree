import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Node } from "../../src";

@Component({
    selector: "app-node",
    templateUrl: "./node.component.html",
    styleUrls: ["./node.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NodeComponent {
    @Input()
    public node: Node;

    public expanded = true;
    public showInput = false;


    get showExpandButton(): boolean {
        return !!this.node.children?.length;
    }

    public comment: string | undefined;

    public setComment(event: string) {
        this.comment = event;
    }

    public expand() {
        this.expanded = !this.expanded;
    }

    public showAddCommentComponent() {
        this.showInput = !this.showInput;
    }

    public removeComent() {
        this.comment = undefined;
    }
}
