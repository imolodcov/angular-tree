import { TestBed } from "@angular/core/testing";
import { Node } from "../../src";
import { TreeService } from "./tree.service";

describe("TreeService", () => {
    let service: TreeService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(TreeService);
    });

    it("should be created", () => {
        expect(service).toBeTruthy();
    });

    it("should be created tree", () => {
        const inputTree: Node[] = [
            { id: "0", name: "node_modules", parentId: "-1" },
            {
                id: "1",
                name: "webpack-dev-middleware",
                parentId: "0"
            },
            {
                id: "2",
                name: "webpack-dotenv-plugin",
                parentId: "0"
            },
            {
                id: "9",
                name: "node_modules",
                parentId: "1"
            }
        ];
        expect(service.makeTree(inputTree)).toEqual([
            {
                id: "0",
                name: "node_modules",
                parentId: "-1",
                status: "default",
                children: [
                    Object({
                        id: "1",
                        name: "webpack-dev-middleware",
                        parentId: "0",
                        status: "default",
                        children: [
                            Object({
                                id: "9",
                                name: "node_modules",
                                parentId: "1",
                                status: "default"
                            })
                        ]
                    }),
                    Object({
                        id: "2",
                        name: "webpack-dotenv-plugin",
                        parentId: "0",
                        status: "default"
                    })
                ]
            }
        ]);
    });

    it("should be changed tree", () => {
        const firstInputTree: Node[] = [
            { id: "0", name: "node_modules", parentId: "-1" },
            {
                id: "1",
                name: "webpack-dev-middleware",
                parentId: "0"
            },
            {
                id: "2",
                name: "webpack-dotenv-plugin",
                parentId: "0"
            },
            {
                id: "9",
                name: "node_modules",
                parentId: "1"
            }
        ];
        const fsecondInputTree: Node[] = [
            { id: "0", name: "qbrt", parentId: "-1" },
            {
                id: "1",
                name: "webpack-dev-bin",
                parentId: "0"
            },
            {
                id: "2",
                name: "webpack-dotenv-plugin",
                parentId: "0"
            },
            {
                id: "3",
                name: "node_modules",
                parentId: "1"
            }
        ];
        service.makeTree(firstInputTree);
        expect(service.makeTree(fsecondInputTree)).toEqual([
            {
                id: "0",
                name: "qbrt",
                parentId: "-1",
                status: "changed",
                children: [
                    Object({
                        id: "1",
                        name: "webpack-dev-bin",
                        parentId: "0",
                        status: "changed",
                        children: [
                            Object({
                                id: "3",
                                name: "node_modules",
                                parentId: "1",
                                status: "added"
                            }),
                            Object({
                                id: "9",
                                name: "node_modules",
                                parentId: "1",
                                status: "removed",
                                children: []
                            })
                        ]
                    }),
                    Object({
                        id: "2",
                        name: "webpack-dotenv-plugin",
                        parentId: "0",
                        status: "changed"
                    })
                ]
            }
        ]);
    });
});
