import { Injectable } from "@angular/core";
import { Node } from "../../src";

@Injectable({
    providedIn: "root"
})
export class TreeService {
    private oldTree: Node[] = [];

    public makeTree(node: Node[]): Node[] {
        let tree: Node[] = [];
        for (const item of node) {
            item.status = "default";
            if (item.parentId === "-1") {
                tree.push(item);
            } else {
                const parent = node.find((el) => el.id === item.parentId);
                if (parent) {
                    if (!Array.isArray(parent?.children)) {
                        parent.children = [];
                    }
                    parent.children.push(item);
                }
            }
        }
        this.compareNode(tree, this.oldTree);
        this.oldTree = JSON.parse(JSON.stringify(tree));
        return tree;
    }

    private compareNode(node: Node[], oldNode: Node[]) {
        node.forEach((item, i, array) => {
            let index: number;
            if (oldNode.length) {
                item.status = "added";
            }
            index = oldNode.findIndex((oldItem) => item.id === oldItem.id);
            if (index >= 0) {
                oldNode[index].status = "changed";
                item.status = "changed";
            }
            if (i === array.length - 1) {
                oldNode
                    .filter((el) => el.status !== "changed")
                    .forEach((el) => {
                        el.status = "removed";
                        el.children = [];
                        node.push(el);
                    });
            }
            if (!item.children) {
                return;
            } else {
                this.compareNode(item.children, oldNode[index]?.children ?? []);
            }
        });
    }
}
