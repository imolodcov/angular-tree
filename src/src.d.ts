export class Node {
    id: string;
    name: string;
    parentId: string;
    status?: nodeStatus;
    children?: Node[];
}

type nodeStatus = "changed" | "removed" | "added" | "default";
